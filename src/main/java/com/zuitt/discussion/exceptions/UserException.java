package com.zuitt.discussion.exceptions;

import com.zuitt.discussion.models.User;

public class UserException extends Exception{
    public UserException(String message){
        super(message);
    }
}
